$(document).ready(function () {
    var fileList = [];
    $(".selectFiles").click(function () {
        $("#file").click();
    });
    $("#file").change(function () {
        imagePreview($("#file")[0].files);
    });
    $("#output").delegate("img", 'click', function () {
        fileList.splice($(this).attr('inArr'), 1);
        $(this).parent().fadeOut().remove();
    });
    function imagePreview(files) {
        var max_count = 4 - fileList.length;
        if (files.length <= max_count) {
            for (var i = 0; i < files.length; i++) {
                if (files[i].size > (1028 * 1028 * 8)) {
                    alert("Невозможно загрузить файл больше 8 МБ");
                    return;
                }
                if (/image.*/.test(files[i].type)) {
                    var td = $('<td />').appendTo("#output");
                    fileList.push(files[i]);
                    var img = $('<img inarr="' + fileList.indexOf(files[i]) + '"/>').appendTo(td);
                    td.append('<span></span>');
                    img.attr('height', 140);
                    img.attr('width', 193);
                    var reader = new FileReader;
                    reader.onload = (function (aImg) {
                        return function (e) {
                            aImg.attr('src', e.target.result);
                        }
                    })(img);
                    reader.readAsDataURL(files[i]);
                }
                else {

                }
            }
        }
        else {
            alert('нельзя загрузить больше 4 файлов');
        }
    }

//==================================================================================================================================
    $("body").delegate("#add_save", "click", function (event) {
        $("#add_save").attr('disabled', 'disabled');
        if (confirm("Вы действительно прикрепили все файлы которые хотели?")) {
            if (fileList.length != 0) {
                for(var i=0;i < fileList.length;i++)
                {

                    var file = fileList[i];
                    fd = new FormData ();
                    xhr = new XMLHttpRequest ();
                    this.xhr = xhr;
                    fd.append('myFile', file);
                    fd.append('description', $("#descr").val());
                    xhr.open("POST","addFoto");
                    xhr.setRequestHeader('X-CSRF-TOKEN', $("#tokenFile").val());
                    xhr.send(fd);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.status == 200)
                        {
                            $('#windowFile').find('h4').text('Файлы успешно загружены');
                            $('#windowFile').modal("show");
                        }
                    };
                    this.xhr.onload = function() {
                        var response = this.responseText;
                        if(response == 'success')
                        {
                            $('#windowFile').find('h4').text('Файлы успешно загружены');
                            $('#windowFile').modal("show");
                        }
                        else
                        {
                            $('#windowFile').find('h4').attr('color','red');
                            $('#windowFile').find('h4').text('Файлы не загружены!!!');
                            $('#windowFile').modal("show");
                        }
                    }
                }
            }
            else
            {
                alert('нет файлов');
            }
        }

    });
    $('#windowClose').click(function(){
        location.reload();
    });

    //$('.Photo')
    $('.prewiew').click(function(){
        var w = $("html").width();
        var h = $("html").height();



        var srcPrewiew = $(this).find('img').attr('src');
        var srcPhoto = srcPrewiew.replace('resizeImage','tmpFiles');
        var description = $(this).find('#inputDescription').val();
        var number = $(this).find('.numberPrewiew').val();
        $('.Photo').find('img').attr('src',srcPhoto);
        $('.Photo').find('p').text(description);
        $('.modalPhoto').find('.numberPhoto').val(number);
        $('.Photo').modal("show");
    });
    $('#buttonNext').click(function(){
        var number = $('.modalPhoto').find('.numberPhoto').val();
        var srcPrewiew = $('.prewiew').find('#' + (parseInt(number)+1)).parent('.prewiew').find('img').attr('src');
        if(srcPrewiew)
        {
            var srcPhoto = srcPrewiew.replace('resizeImage','tmpFiles');
            var description = $('.prewiew').find('#' + (parseInt(number)+1)).parent('.prewiew').find('#inputDescription').val();

            $(this).siblings('img').attr('src',srcPhoto);
            
            $('.description p').text(description);
            $('.modalPhoto').find('.numberPhoto').val(parseInt(number)+1);
        }
        else
        {
            var srcPrewiew = $('.prewiew').find('#1').parent('.prewiew').find('img').attr('src');
            var srcPhoto = srcPrewiew.replace('resizeImage','tmpFiles');
            var description = $('.prewiew').find('#1').parent('.prewiew').find('#inputDescription').val();

            $(this).siblings('img').attr('src',srcPhoto);
            $('.description p').text(description);
            $('.modalPhoto').find('.numberPhoto').val(1);
        }
    });
    $('#buttonBack').click(function(){
        var number = $('.modalPhoto').find('.numberPhoto').val();
        var srcPrewiew = $('.prewiew').find('#' + (parseInt(number)-1)).parent('.prewiew').find('img').attr('src');
        if(srcPrewiew)
        {
            var srcPhoto = srcPrewiew.replace('resizeImage','tmpFiles');
            var description = $('.prewiew').find('#' + (parseInt(number)-1)).parent('.prewiew').find('#inputDescription').val();

            $(this).siblings('img').attr('src',srcPhoto);
            $('.description p').text(description);
            $('.modalPhoto').find('.numberPhoto').val(parseInt(number)-1);
        }
        else
        {
            var srcPrewiew = $('.prewiew').find('#' + $(".prewiew").length).parent('.prewiew').find('img').attr('src');
            var srcPhoto = srcPrewiew.replace('resizeImage','tmpFiles');
            var description = $('.prewiew').find('#' + $(".prewiew").length).parent('.prewiew').find('#inputDescription').val();

            $(this).siblings('img').attr('src',srcPhoto);
            $('.description p').text(description);
            $('.modalPhoto').find('.numberPhoto').val($(".prewiew").length);
        }
    });
    $('#deletePhoto').click(function(){
        var src = $('.modalPhoto').find('img').attr('src');
        console.log(src);
        $('.askDel').modal("show");
    });
    $('#delYes').click(function(data){
        var src = $('.modalPhoto').find('img').attr('src');
        $.get('deletePhoto',{src : src},function(data){
            console.log(data);
            if(data == 'success')
            {
                $('.confirmDelPhoto').find('h4').text('Фото успешно удалено');
                $('.confirmDelPhoto').modal('show');
            }
            else
            {
                $('.confirmDelPhoto').find('h4').text('Ошибка удаления');
                $('.confirmDelPhoto').modal('show');
            }
        });
    });
    $('#hideDelButton').click(function(){
        location.reload();
    });
    $('#editPhoto').click(function(){
        var text = $('.description p').text();
        $('.editDescription textarea').text(text);
        $('.editDescription').modal("show");
    });
    $('#saveEditDescription').click(function(){
        var text = $('.editDescription textarea').val();
        console.log(text);
        var src = $('.modalPhoto').find('img').attr('src');
        $('.description p').text(text);
        $.get('saveEditDescription',{src : src,text : text},function(data){
            console.log(data);
        });
    });
    $('#AddPrice').click(function(){
        var category = $('#tdSelect option:checked').val();
        var name = $('#tdName').val();
        var price = $('#Price').val();
        $.get('addPrice',{category : category, name : name, price : price},function(){
            location.reload();
        });

    });
    $('#AddCategory').click(function(){
        var category = $('#categoryName').val();
        $.get('addCategory',{category : category},function(data){
            location.reload();
        });
    });

    $('#buttonDeleteService').click(function()
    {
        $('.askDelService').modal("show");
    });

    $('#delService').click(function(){

        $.get('serviceDelete',{
            service : $('#selectService').val(),
            category : $('#selectCategory').val(),
        },function(data){
            if(data == 'success')
            {
                location.reload();
            }
        });
    });




    $('#selectCategory').change(function(){
        $.get('loadService',{category:$('#selectCategory').val()},function(data){
            $('#selectService').html('');
            for (var i = 0;i < data.length;i++)
            {
                $('#selectService').append('<option>' + data[i].name + '</option>');
            }
            $('#buttonEditService').click(function(){
                $.get('loadEditPrice',{
                    service : $('#selectService').val(),
                    category : $('#selectCategory').val()
                },function(data){
                    //console.log(data.id);
                    var tr = '<td class="tdPrice">Название:<input id="newNameService" type="text" value="' + data.name + '"></td><td class="tdPrice">Цена:<input id="newPriceService" type="text" value="' + data.price + '"></td><td class="tdPrice" id="editService">Сохранить</td><td class="tdPrice" style="background-color: white"></tr>';
                    $('#showEditInputService').html(tr);
                    $('#editService').click(function(){
                        var price = $('#newPriceService').val();
                        var service = $('#newNameService').val();
                        $.get('editService',{id : data.id,price : price,service : service},function(data){
                            //console.log(data);
                            if(data == 'success')
                            {
                                location.reload();
                            }
                        });
                    });
                });
            });
        });
    });
    $('#editCategory').click(function(){
        var category = $('#selectEditCategory').val();
        var tr = '<td class="tdPrice">Название:<input id="newNameCategory" type="text" value="' + category + '"></td><td class="tdPrice" id="saveCategory">Сохранить</td><td class="tdPrice" style="background-color: white"></td<td class="tdPrice" style="background-color: white"></td>';
        $('#showEditInputCategory').html(tr);
        $('#saveCategory').click(function(){
            $.get('editCategory',{old : category,new : $('#newNameCategory').val()},function(data){
                if(data == 'success')
                {
                    location.reload();
                }
            });
        });

    });

    $('#buttonDeleteCategory').click(function()
    {
        $('.askDelCategory').modal("show");
    });

    $('#delCategory').click(function(){
        $.get('deleteCategory',{category : $('#selectEditCategory').val()},function(data){
            if(data == 'success')
            {
                location.reload();
            }
        });
    });

    $("#addNewContact").click(function(){
        $.get('addNewContact',{contact : $("#newTextContact").val(),type : $('#contactType').val()},function(data){
            if(data == 'success')
            {
                location.reload();
            }

        });
    });
    $('.buttonDelContacts').click(function(){
        $.get('delContact',{ value : $(this).find('input').val()},function(data){
            if(data == 'success')
            {
                location.reload()
            }
        });
    });













});