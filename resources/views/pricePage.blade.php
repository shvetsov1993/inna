@extends('layouts.app')

@section('content')
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        {{--<div class="panel-heading">Welcome</div>--}}

                        <div class="panel-body">
                            <table id="tablePrice" class="table table-striped">
                                @foreach($var as $category => $value)
                                    <tr><td colspan="2" class="headerPrice">{{ $category }}</td></tr>
                                    @foreach($value as $key=> $val)
                                        <tr class="trPrice">
                                            <td>{{ $val['name'] }}</td>
                                            <td>{{ $val['price'] }}</td>
                                        </tr>
                                    @endforeach

                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    {{ $var['srthht'][0] }}--}}
@endsection