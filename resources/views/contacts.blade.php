@extends('layouts.app')

@section('content')
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        {{--<div class="panel-heading">Welcome</div>--}}

                        <div class="panel-body">
                            <table id="tableContact" class="table">
                                @foreach($contacts as $key => $value)
                                    <tr>
                                        <td>{{ $value['type'] }}</td>
                                        <td>{{ $value['value'] }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection