@extends('layouts.app')

@section('content')
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        {{--<div class="panel-heading">Welcome</div>--}}

                        <div class="panel-body">

                                <table style="margin-top: 20px;margin-right:auto;margin-left:auto">
                                    <tr>
                                        {{-- */ $i=0; /* --}}
                                        {{-- */ $number=1; /* --}}
                                        @foreach($result as $key => $value)
                                            <td class="prewiew">
                                                <img width="" src="{{ $value['prewiew'] }}">
                                                <input id="inputDescription" type="hidden" value="{{ $value['description'] }}">
                                                <input id="{{ $number }}" class="numberPrewiew" type="hidden" value="{{ $number }}">

                                            </td>
                                            @if($i == 3)
                                                {{-- */ $i=0; /* --}}
                                    </tr><tr>
                                        @else
                                            {{-- */ $i++; /* --}}
                                        @endif
                                        {{-- */ $number++; /* --}}
                                        @endforeach
                                    </tr>
                                </table>


                            <div data-backdrop="false" class="modal fade Photo">
                                    <button id="closeModal" data-dismiss="modal">X</button>
                                <div class="modalPhoto col-xs-8">
                                    {{--<button style="color:black;z-index: 9999999999999999">rgns</button>--}}
                                    {{--<div style="display: inline-block;background-color: #6f1400;width: 15px;z-index: 9999999999999999""></div>--}}
                                    <img class="col-xs-12" src=''>
                                    <br />

                                        <button id="buttonNext" class="col-md-4"><b>Вперед</b></button>
                                        <button id="buttonBack" class="col-md-4"><b>Назад</b></button>
                                    <input class="numberPhoto" type="hidden" value="">
                                </div>
                                <div class="description col-xs-3">
                                    <p></p>
                                    @yield('adminEdit')
                                </div>
                            </div>

                            <div class="modal fade askDel">
                                <div class="modal-content">
                                    <h4>Вы действительно хотите удалить фото?
                                        <button id="delYes" data-dismiss="modal">Да</button>
                                        <button id="delNo" data-dismiss="modal">Нет</button>
                                    </h4>

                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>

                            <div class="modal fade confirmDelPhoto">
                                <div class="modal-content">
                                    <h4></h4>
                                    <button id="hideDelButton" data-dismiss="modal">Ок</button>
                                </div>
                                <div class="modal-footer">

                                </div>
                            </div>

                            <div class="modal fade editDescription">
                                <div class="modal-content">
                                    <textarea></textarea>
                                    <div>
                                        <button id="saveEditDescription" data-dismiss="modal">Сохранить</button>
                                        <button id="cancelEditDescription" data-dismiss="modal">Отмена</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
