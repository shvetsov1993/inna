@extends('layouts.app')

@section('content')
    <div class="home">
        <table id="delContact" class="table">
            @foreach($contacts as $key => $value)
                <tr>
                    <td class="tdDelContacts typeContact">{{ $value['type'] }}</td>
                    <td class="tdDelContacts valueContact">{{ $value['value'] }}</td>
                    <td class="tdDelContacts buttonDelContacts">
                        Удалить
                    <input type="hidden" value="{{ $value['value'] }}">
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection