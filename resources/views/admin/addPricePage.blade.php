@extends('layouts.app')

@section('content')
    <div>
        <table id="addPricePage">
            <caption>Добавление цен</caption>
            <tr>
                <td class="tdPrice">Выберете категорию:</td>
                <td class="tdPrice">Название услуги:</td>
                <td class="tdPrice">Цена:</td>
                <td class="tdPrice">Нажмите добавить</td>
            </tr>
            <tr>
                <td class="tdPrice"><select id="tdSelect">
                        @foreach($category as $key => $val)
                            <option>{{ $val['category'] }}</option>
                        @endforeach
                    </select></td>
                <td class="tdPrice"><input id="tdName" type="text"></td>
                <td class="tdPrice"><input id="Price" type="text"></td>
                <td class="tdPrice" id="AddPrice">Добавить</td>
            </tr>
        </table>
    </div>
    <div class="buferTd"></div>
    <div>
        <table id="addCategoryPage">
            <tr>
                <td class="tdCategory" id="tdHeaderCategory">Добавьте категорию</td>
                <td class="tdCategory"><input type="text" id="categoryName" value=""></td>
                <td class="tdCategory" id="AddCategory">Добавить</td>
            </tr>
        </table>
    </div>
    <div class="buferTd" style="height: 450px"></div>
@endsection