@extends('layouts.app')

@section('content')
    <div>
        <table id="editPricePage">
            <caption>Редактирование цен</caption>
            <tr>
                <td class="tdPrice">Выберете категорию:</td>
                <td class="tdPrice">Выберете услугу:</td>
                <td class="tdPrice">Новая цена:</td>
                <td class="tdPrice">Нажмите изменить</td>
            </tr>
            <tr>
                <td class="tdPrice">
                    <select id="selectCategory">
                        @foreach($category as $key => $val)
                            <option>{{ $val['category'] }}</option>
                        @endforeach
                    </select>
                </td>
                <td class="tdPrice">
                    <select id="selectService"></select>
                </td>
                <td class="tdPrice" id="buttonEditService">Изменить</td>
                <td class="tdPrice" id="buttonDeleteService">Удалить</td>
            </tr>
            <tr id="showEditInputService"></tr>
        </table>
    </div>
    <div class="buferTd"></div>
    <div>
        <table id="editCategoryPage">
            <caption>Редактирование категорий</caption>
            <tr>
                <td class="tdPrice">
                    <select id="selectEditCategory">
                        @foreach($category as $key => $val)
                            <option>{{ $val['category'] }}</option>
                        @endforeach
                    </select>
                </td>
                <td class="tdPrice" id="editCategory">Изменить</td>
                <td class="tdPrice" id="buttonDeleteCategory">Удалить</td>
                <td class="tdPrice" style="background-color: white"></td>
            </tr>
            <tr id="showEditInputCategory"></tr>
        </table>
    </div>
    <div class="modal fade askDelService">
        <div class="modal-content">
            <h4>Вы действительно хотите удалить услугу?
                <button id="delService" data-dismiss="modal">Да</button>
                <button id="delNo" data-dismiss="modal">Нет</button>
            </h4>

        </div>
        <div class="modal-footer">

        </div>
    </div>
    <div class="modal fade askDelCategory">
        <div class="modal-content">
            <h4>Вы действительно хотите удалить категорию?
                <button id="delCategory" data-dismiss="modal">Да</button>
                <button id="delNo" data-dismiss="modal">Нет</button>
            </h4>

        </div>
        <div class="modal-footer">

        </div>
    </div>
    <div class="buferTd" style="height: 450px"></div>
@endsection