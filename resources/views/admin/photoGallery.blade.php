@extends('photoGallery')

@section('adminEdit')
    <div>
        <button id="deletePhoto">Удалить</button>
        <button id="editPhoto">Редактировать</button>
        <button style="display: none" id="saveDescription">Сохранить</button>
    </div>
@endsection