@extends('layouts.app')

@section('content')
    <div class="home">
        <table id="newContact" class="table table-striped">
            <tr>
                <td>Тип</td>
                <td>Значение</td>
            </tr>
            <tr>
                <td>
                    <select id="contactType">
                        <option selected>Номер телефона</option>
                        <option>Email</option>
                    </select>
                </td>
                <td><input id="newTextContact" type="text"></td>
            </tr>
            <tr>
                <td id="addNewContact" colspan="2">Добавить</td>
            </tr>
        </table>
    </div>
@endsection