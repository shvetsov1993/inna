@extends('layouts.app')

@section('content')
    <div class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Admin</div>

                        <div class="panel-body">
                            <table class="adminTable">
                                <tr class="adminTr">
                                    <td class="tdHeader">Фото</td>
                                    <td class="tdHeader">Цены</td>
                                    <td class="tdHeader">Контакты</td>
                                </tr>
                                <tr class="adminTr">
                                    <td class="adminTd"><a href="{{ url('buttonEditFoto') }}">Редактировать</a></td>
                                    <td class="adminTd"><a href="{{ url('buttonEditPrice') }}">Редактировать</a></td>
                                    <td class="adminTd"><a href="{{ url('buttonEditContacts') }}">Удалить</a></td>
                                </tr>
                                <tr class="adminTr">
                                    <td class="adminTd"><a href="{{ url('buttonAddFoto') }}">Добавить</a></td>
                                    <td class="adminTd"><a href="{{ url('buttonAddPrice') }}">Добавить</a></td>
                                    <td class="adminTd"><a href="{{ url('buttonAddContacts') }}">Добавить</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

