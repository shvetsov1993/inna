@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Добавление фото</div>

                    <div class="panel-body">
                        <div id="parentTd">
                            <input id="tokenFile" type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="idFoto" type="hidden" value="{time()}"/>
                            <button class="selectFiles"><b>Выбрать рисунок</b></button>
                            <input style="display:none" id="file" type="file" multiple="true"/><br />
                            <table id="output">
                                <tr></tr>
                            </table>
                            Введите описание фото:<br />
                            <textarea id="descr" style="width:100%;height:150px"></textarea><br />
                        </div>
                        <button id="add_save"><b>Добавить</b></button>

                        {{--<button class="btn btn-info btn-lg" type="button" data-toggle="modal" data-target="#errorFile"></button>--}}
                        <div id="windowFile" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title"></h4>
                                    </div>
                                    <div class="modal-footer"><button id="windowClose" class="btn btn-default" type="button" data-dismiss="modal">OK</button></div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection