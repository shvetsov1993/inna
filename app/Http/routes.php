<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    //Аутентификация
    //==============================================================
    Route::auth();
    //==============================================================
    //Разное
    //==============================================================
    Route::get('photoGallery','PhotoController@loadPhoto');
    Route::get('contacts','EasyController@contacts');
    Route::get('price','EasyController@price');
    Route::get('/', function () {
        return view('welcome');
    });
    //==============================================================
    //Админка
    //==============================================================
    Route::group(['middleware' => ['auth']], function(){
        Route::group(['middleware' => ['admin']], function(){
            Route::get('adminPage','Admin\AdminController@index');
            Route::get('buttonAddFoto','Admin\AdminController@buttonAddFoto');
            Route::get('buttonAddPrice','Admin\AdminController@buttonAddPrice');
            Route::get('buttonAddContacts','Admin\AdminController@buttonAddContacts');
            Route::get('buttonEditFoto','Admin\AdminController@buttonEditFoto');
            Route::get('buttonEditPrice','Admin\AdminController@buttonEditPrice');
            Route::get('buttonEditContacts','Admin\AdminController@buttonEditContacts');
            Route::get('deletePhoto','Admin\AdminController@deletePhoto');
            Route::get('saveEditDescription','Admin\AdminController@saveEditDescription');
            Route::post('addFoto','Admin\AdminController@addFoto');
            Route::get('addPrice','Admin\AdminController@addPrice');
            Route::get('addCategory','Admin\AdminController@addCategory');
            Route::get('addNewContact','Admin\AdminController@addNewContact');
            Route::get('editService','Admin\AdminController@editService');
            Route::get('loadEditPrice','Admin\AdminController@loadEditPrice');
            Route::get('serviceDelete','Admin\AdminController@serviceDelete');
            Route::get('loadService','Admin\AdminController@loadService');
            Route::get('editCategory','Admin\AdminController@editCategory');
            Route::get('deleteCategory','Admin\AdminController@deleteCategory');
            Route::get('delContact','Admin\AdminController@delContact');
        });
    });
    //==============================================================
});
