<?php
namespace App\Http\Controllers;

use App\Models\Foto;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PhotoController extends Controller
{
    public function loadPhoto()
    {
        $result = [];
        $allPhoto = Foto::all();
        foreach ($allPhoto as $key => $value) {
            //            $photoData = Storage::disk('files')->get($value['name'] . '.' . $value['extension']);
            //            $photoPublic = public_path('tmpFiles/' . $value['name'] . '.' . $value['extension']);
            $pathPrewiew = 'resizeImage/' . $value['name'] . '.' . $value['extension'];
            $pathPhoto = 'tmpFiles/' . $value['name'] . '.' . $value['extension'];
            //            File::put($photoPublic,$photoData);
            $description = $value['description'];
            $result[] = [
                'prewiew'     => $pathPrewiew,
                'photo'       => $pathPhoto,
                'description' => $description,
            ];
        }

        //dd($result);
        return view('photoGallery')->with('result', $result);
    }
}


?>
