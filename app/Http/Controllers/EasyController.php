<?php
namespace App\Http\Controllers;


use App\Models\Price;
use App\Models\Contact;

class EasyController extends Controller
{
    public function price()
    {
        $var = Price::all()->groupBy('category');
        return view('pricePage')->with('var',$var);
    }

    public function contacts()
    {
        $contacts = Contact::all();
        return view('contacts')->with('contacts',$contacts);
    }




}
?>
