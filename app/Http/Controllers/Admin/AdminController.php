<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Foto;
use App\Models\Price;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.adminPage');
    }

    public function buttonAddFoto()
    {
        return view('admin.addFotoPage');
    }

    public function buttonEditFoto()
    {
        $result = [];
        $allPhoto = Foto::all();
        foreach ($allPhoto as $key => $value) {
            $pathPrewiew = 'resizeImage/' . $value['name'] . '.' . $value['extension'];
            $pathPhoto = 'tmpFiles/' . $value['name'] . '.' . $value['extension'];
            $description = $value['description'];
            $result[] = [
                'prewiew' => $pathPrewiew,
                'photo' => $pathPhoto,
                'description' => $description,
            ];
        }

        return view('admin.photoGallery')->with('result', $result);
    }

    public function buttonEditPrice()
    {
        $category = Category::all();
//        $price = Price::all();
        return view('admin.editPricePage')->with('category',$category);
//            [
//                'category' => $category,
//                'price'    => $price,
//            ]
//        );
    }

    public function buttonEditContacts()
    {
        $contacts = Contact::all();
        return view('admin.editContacts')->with('contacts',$contacts);
    }


    public function loadService(Request $request)
    {
        $category = $request->input('category');
        $price = Price::where('category',$category)->get();
        return $price;
    }


    public function addFoto(Request $request)
    {
        if ($request->hasFile('myFile')) {
            $disk = Storage::disk('files');
            if ($request->file('myFile')->isValid()) {
                $fileHash = md5(time() . $request->file('myFile') . 'remont');
                $fileName = $fileHash . '.' . $request->file('myFile')->getClientOriginalExtension();
                $disk->put($fileName, File::get($request->file('myFile')));
                $record = $this->addInTableFotos(
                    $request->file('myFile')->getClientOriginalName(),
                    $fileHash,
                    $request->file('myFile')->getClientOriginalExtension(),
                    $request->input('description')
                );
                \Image::make($disk->get($fileName))->resize(1000, 750)->save(public_path('tmpFiles/' . $fileName));
                \Image::make($disk->get($fileName))->resize(200, 150)->save(public_path('resizeImage/' . $fileName));

                return $record;
            }
        }
        else {
            return 'file not';
        }
    }

    public function addInTableFotos($realName, $name, $extension, $description)
    {
        $model = new Foto;
        $model->name = $name;
        $model->realName = $realName;
        $model->extension = $extension;
        $model->description = $description;
        $status = $model->save() ? 'success' : 'error';

        return $status;
    }

    public function deletePhoto(Request $request)
    {
        $src = $request->input('src');
        $fileName = $this->getNamePhoto($src);
        $status = Foto::where('name', $fileName)->delete() ? 'success' : 'error';
        Storage::disk('files')->delete(str_replace('tmpFiles/', '', $src));
        File::delete(public_path($src));
        File::delete(public_path(str_replace('tmpFiles/', 'resizeImage/', $src)));

        return $status;
    }

    public function getNamePhoto($src)
    {
        $file = str_replace('tmpFiles/', '', $src);
        $pocket = explode('.', $file);
        $fileName = $pocket[0];

        return $fileName;
    }

    public function saveEditDescription(Request $request)
    {
        $src = $request->input('src');
        $fileName = $this->getNamePhoto($src);
        $status = Foto::where('name', $fileName)->update(
            ['description' => $request->input('text')]
        ) ? 'success' : 'error';

        return $status;
    }

    public function buttonAddPrice()
    {
        $category = Category::all();

        return view('admin.addPricePage')->with('category', $category);
    }
    public function buttonAddContacts()
    {
        $contacts = Contact::all();

        return view('admin.addContactsPage')->with('Contacts', $contacts);
    }

    public function addPrice(Request $request)
    {
        //        dd($request->all());


        $model = new Price();
        $model->name = $request->input('name');
        $model->category = $request->input('category');
        $model->price = $request->input('price');
        $status = $model->save() ? 'success' : 'error';

        return $status;
    }

    public function addCategory(Request $request)
    {
        $model = new Category();
        $model->category = $request->input('category');
        $status = $model->save() ? 'success' : 'error';

        return $status;
    }

    public function editService(Request $request)
    {
        $price = $request->input('price');
        $service = $request->input('service');
        $id = $request->input('id');
        $status = Price::where('id',$id)->update([
            'name' => $service,
            'price' => $price,
        ]) ? 'success' : 'error';

        return $status;
    }

    public function loadEditPrice(Request $request)
    {
        $category = $request->input('category');
        $service = $request->input('service');
        $result = Price::where([
            'category' => $category,
            'name'    => $service,
        ])->get()->first();

        return $result;
    }

    public function serviceDelete(Request $request){
        $status = Price::where([
            'category' => $request->input('category'),
            'name'    => $request->input('service'),
        ])->delete() ? 'success' : 'error';
        return $status;
    }

    public function editCategory(Request $request)
    {
        $old = $request->input('old');
        $new = $request->input('new');
        Category::where('category',$old)->update(['category' => $new]);
        $status = Price::where('category',$old)->update(['category' => $new]) ? 'success' : 'error';
        return $status;
    }

    public function deleteCategory(Request $request)
    {
        $category = $request->input('category');
        Price::where('category',$category)->delete();
        $status = Category::where('category',$category)->delete() ? 'success' : 'error';
        return $status;
    }

    public function addNewContact(Request $request)
    {
        $contact = $request->input('contact');
        $type = $request->input('type');
        if($request->input('type') == 'Номер телефона');
        {
            $type = "number";
        }
        if($request->input('type') == 'Email')
        {
            $type = "Email";
        }
        $model = new Contact();
        $model->value = $contact;
        $model->type = $type;
        $status = $model->save() ? 'success' : 'error';

        return $status;
    }
    public function delContact(Request $request)
    {
        $contact = $request->input('value');
        $status = Contact::where([
            'value' => $contact
        ])->delete() ? 'success' : 'error';
        return $status;
    }
}

?>